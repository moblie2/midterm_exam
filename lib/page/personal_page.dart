import 'package:flutter/material.dart';

class PersonalPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
    body: buildBodyWidget,
    appBar: AppBar(
        title: Text('Personal'),
      centerTitle: true,
      backgroundColor: Colors.pink.shade100,
    ),
  );
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          color: Colors.pink.shade100,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          color: Colors.pink.shade100,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}


Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          color: Colors.pink.shade100,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          color: Colors.pink.shade100,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget mobilePhoneListTile() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("0863870662"),
    subtitle: Text("MIND"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.pink.shade100,
      onPressed: () {},
    ),
  );
}

Widget otherPhoneListTitle() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("0890808667"),
    subtitle: Text("PARENT(MOM)"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.pink.shade100,
      onPressed: () {},
    ),
  );
}

Widget mailListTitle() {
  return ListTile(
    leading: Icon(Icons.mail),
    title: Text("63160218@go.buu.ac.th"),
    subtitle: Text("WORK"),
  );
}

Widget locationListTitle() {
  return ListTile(
    leading: Icon(Icons.location_on) ,
    title: Text("Erawan Lopburi 15000"),
    subtitle: Text("HOME"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      color: Colors.pink.shade100,
      onPressed: () {},
    ),
  );
}

Widget bloodtype() {
  return ListTile(
    leading: Icon(Icons.bloodtype),
    title: Text("Grop: A"),
  );
}

Widget birthday() {
  return ListTile(
    leading: Icon(Icons.cake),
    title: Text("22 JUNE 2002"),
  );
}
Widget faculties() {
  return ListTile(
    leading: Icon(Icons.menu_book_outlined),
    title: Text("Faculty of Informatice"),
    subtitle: Text("Computer Science (CS)"),
  );
}
Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildEmailButton(),
      buildDirectionsButton(),
    ],
  );
}

// var buildAppBarWidget = AppBar(
//   leading: Icon(
//     Icons.arrow_back,
//   ),
//   title: Text('Personal'),
//   centerTitle: true,
//   backgroundColor: Colors.pink.shade100,
// );

var buildBodyWidget = ListView(
  children: <Widget>[
    Column(
      children: <Widget>[
        Container(
          width: double.infinity,
          height: 250,
          child: Image.network(
            "https://scontent.fbkk30-1.fna.fbcdn.net/v/t39.30808-6/327268239_755823032610753_2441246779163147456_n.jpg?_nc_cat=102&ccb=1-7&_nc_sid=730e14&_nc_ohc=wJpd9_C--20AX8zj6wj&_nc_ht=scontent.fbkk30-1.fna&oh=00_AfC-eyR9X3gil5dQjtUdWC0Q7aUmEElQKqK2j69EwWsY2g&oe=63D9A526",
            fit: BoxFit.cover,
          ),
        ),
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Waratchaya Roengjai (63160218)",
                  style: TextStyle(fontSize: 30),
                ),
              )
            ],
          ),
        ),
        Divider(
          color: Colors.black,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8, bottom: 8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
            ],
          ),
        ),

        Divider(color: Colors.grey
        ),
        Container(
          margin: const EdgeInsets.only(top: 8, bottom: 8),
          child: Theme(
            data: ThemeData(
              iconTheme: IconThemeData(
                color: Colors.pink,
              ),
            ),
            child: profileActionItems(),
          ),
        ),
        Divider(color: Colors.grey),
        mobilePhoneListTile(),
        otherPhoneListTitle(),
        mailListTitle(),
        locationListTitle(),
        bloodtype(),
        birthday(),
        faculties(),
      ],
    )
  ],
);

