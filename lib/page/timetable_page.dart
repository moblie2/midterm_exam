import 'package:flutter/material.dart';

class TimetablePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
      body: buildWidget,
    //drawer: NavigationDrawerWidget(),
    appBar: AppBar(
      title: Text('Timetable'),
      centerTitle: true,
      backgroundColor: Colors.blue.shade200,
    ),
  );
}

var buildWidget = ListView(
  children: <Widget>[
    Column(
      children: <Widget>[
        Container(
          width: double.infinity,
          height: 250,
          child: Image.network(
            "https://cdn.discordapp.com/attachments/881538016856920094/1069009523592941568/image.png",
            fit: BoxFit.cover,
          ),
        ),
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "TIME TABLE",
                  style: TextStyle(fontSize: 30),
                ),
              )
            ],
          ),
        ),
        Divider(
          color: Colors.grey,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8, bottom: 8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
            ],
          ),
        ),

        Divider(color: Colors.grey
         ),
        Container(
          margin: const EdgeInsets.only(top: 8, bottom: 8),
          child: Theme(
            data: ThemeData(
              iconTheme: IconThemeData(
                color: Colors.pink,
              ),
            ),
            child: collegepersonal(),
          ),
        ),
        Divider(color: Colors.grey),
        subject1(),
        subject2(),
        subject3(),
        subject4(),
        subject5(),
      ],
    )
  ],
);

Widget collegepersonal() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      personButton(),
      subjectButton(),
    ],
  );
}

Widget personButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.person,
          color: Colors.blue.shade200,
        ),
        onPressed: () {},
      ),
      Text("Person"),
    ],
  );
}

Widget subjectButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.menu_book_outlined,
          color: Colors.blue.shade200,
        ),
        onPressed: () {},
      ),
      Text("Subject"),
    ],
  );
}

Widget subject1() {
  return ListTile(
    leading: Icon(Icons.menu_book_outlined),
    title: Text("Web Programming"),
    subtitle: Text("88624359-59"),
  );
}

Widget subject2() {
  return ListTile(
    leading: Icon(Icons.menu_book_outlined),
    title: Text("Object-Oriented Analysis and Design"),
    subtitle: Text("88624459-59"),
  );
}

Widget subject3() {
  return ListTile(
    leading: Icon(Icons.menu_book_outlined),
    title: Text("Multimedia Programming for Multiplatforms"),
    subtitle: Text("88634259-59"),
  );
}

Widget subject4() {
  return ListTile(
    leading: Icon(Icons.menu_book_outlined),
    title: Text("Software Testing"),
    subtitle: Text("88624559-59"),
  );
}

Widget subject5() {
  return ListTile(
    leading: Icon(Icons.menu_book_outlined),
    title: Text("Mobile Application Development I"),
    subtitle: Text("88634459-59"),
  );
}