import 'package:flutter/material.dart';

class ResultsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
    body: buildBody,
    appBar: AppBar(
      title: Text('Results'),
      centerTitle: true,
      backgroundColor: Colors.blue.shade200,
    ),
  );
}

var buildBody = ListView(
  children: <Widget>[
    Column(
      children: <Widget>[
        Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "ภาคการศึกษาที่ 1/2563",
                    style: TextStyle(fontSize: 30),
                  ),
                )
              ],
            ),
        ),
        Container(
          width: double.infinity,
          height: 250,
          child: Image.network(
            "https://cdn.discordapp.com/attachments/881538016856920094/1069016468315979867/image.png",
            fit: BoxFit.cover,
          ),
        ),

        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "ภาคการศึกษาที่ 2/2563",
                  style: TextStyle(fontSize: 30),
                ),
              )
            ],
          ),
        ),
        Container(
          width: double.infinity,
          height: 250,
          child: Image.network(
            "https://cdn.discordapp.com/attachments/881538016856920094/1069016949218095254/image.png",
            fit: BoxFit.cover,
          ),
        ),

        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "ภาคการศึกษาที่ 1/2564",
                  style: TextStyle(fontSize: 30),
                ),
              )
            ],
          ),
        ),
        Container(
          width: double.infinity,
          height: 250,
          child: Image.network(
            "https://cdn.discordapp.com/attachments/881538016856920094/1069017146014834728/image.png",
            fit: BoxFit.cover,
          ),
        ),

        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "ภาคการศึกษาที่ 2/2564",
                  style: TextStyle(fontSize: 30),
                ),
              )
            ],
          ),
        ),
        Container(
          width: double.infinity,
          height: 250,
          child: Image.network(
            "https://cdn.discordapp.com/attachments/881538016856920094/1069017362457690272/image.png",
            fit: BoxFit.cover,
          ),
        ),

        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "ภาคการศึกษาที่ 1/2565",
                  style: TextStyle(fontSize: 30),
                ),
              )
            ],
          ),
        ),
        Container(
          width: double.infinity,
          height: 250,
          child: Image.network(
            "https://cdn.discordapp.com/attachments/881538016856920094/1069017541172797500/image.png",
            fit: BoxFit.cover,
          ),
        ),
      ],
    )
  ],
);