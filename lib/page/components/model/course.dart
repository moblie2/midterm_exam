import 'package:flutter/material.dart' show Color;

class Course {
  final String title, description, iconSrc;
  final Color color;

  Course({
    required this.title,
    this.description = 'Build and animate an iOS app from scratch',
    this.iconSrc = "assets/icons/ios.svg",
    this.color = const Color(0xFFBD9306),
  });
}

final List<Course> courses = [
  Course(
    title: "หลักสูตรที่เปิดสอน",
  ),
  Course(
    title: "ตรวจสอบจบ",
    color: const Color(0xFF057070),
  ),
];

final List<Course> recentCourses = [
  Course(title: "ขอเชิญนิสิตร่วมทำแบบประเมินความคิดเห็น"),
  Course(
    title: "เสนอความคิดเห็นของนิสิต",
    color: const Color(0xFF10B295),
  ),
  Course(title: "ทำบัตรนิสิตรูปแบบใหม่"),
  Course(
    title: "ยื่นคำร้อง",
    color: const Color(0xFF10B295),
  ),
];