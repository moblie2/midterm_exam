import 'package:flutter/material.dart';

class NotificationsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
    body: buildBody,
    appBar: AppBar(
      title: Text('Notifications'),
      centerTitle: true,
      backgroundColor: Colors.red.shade200,
    ),
  );
}

var buildBody = ListView(
  children: <Widget>[
    Column(
      children: <Widget>[
        // Container(
        //   width: double.infinity,
        //   height: 250,
        //   child: Image.network(
        //     "https://cdn.discordapp.com/attachments/881538016856920094/1069009523592941568/image.png",
        //     fit: BoxFit.cover,
        //   ),
        // ),
        // Container(
        //   child: Row(
        //     mainAxisAlignment: MainAxisAlignment.start,
        //     children: <Widget>[
        //       Padding(
        //         padding: const EdgeInsets.all(8.0),
        //         child: Text(
        //           "TIME TABLE",
        //           style: TextStyle(fontSize: 30),
        //         ),
        //       )
        //     ],
        //   ),
        // ),
        Divider(color: Colors.grey),
        subject1(),
        // Container(
        //   margin: const EdgeInsets.only(top: 8, bottom: 8),
        //   child: Row(
        //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //     children: <Widget>[
        //     ],
        //   ),
        // ),

        Divider(color: Colors.grey),
        subject2(),
        // Container(
        //   margin: const EdgeInsets.only(top: 8, bottom: 8),
        //    child: Theme(
          //   data: ThemeData(
          //     iconTheme: IconThemeData(
          //       color: Colors.pink,
          //     ),
          //   ),
          // ),
        // ),
        Divider(color: Colors.grey),
        subject3(),
        Divider(color: Colors.grey),
        subject4(),
        Divider(color: Colors.grey),
        subject5(),
        Divider(color: Colors.grey),
        subject6(),
        Divider(color: Colors.grey),
        subject7(),
        Divider(color: Colors.grey),
        subject8(),
        Divider(color: Colors.grey),
        subject9(),
        Divider(color: Colors.grey),
        subject10(),
        Divider(color: Colors.grey),
        // subject(),
      ],
    )
  ],
);

Widget subject1() {
  return ListTile(
    leading: Icon(Icons.notifications_rounded),
    title: Text("30 Dec 2022 at 15:00-16:00 Moblie Application Development I at room: IF-3C01"),
    subtitle: Text("30 Dec 2022 14:45"),
  );
}

Widget subject2() {
  return ListTile(
    leading: Icon(Icons.notifications_rounded),
    title: Text("28 Dec 2022 at 10:00-11:50 Moblie Application Development I at room: IF-4C01"),
    subtitle: Text("38 Dec 2022 09:46"),
  );
}

Widget subject3() {
  return ListTile(
    leading: Icon(Icons.notifications_off),
    title: Text("21 Dec 2022 at 17:00-18:50 Web Programming at room: IF-3C01"),
    subtitle: Text("21 Dec 2022 16:46"),
  );
}

Widget subject4() {
  return ListTile(
    leading: Icon(Icons.notifications_rounded),
    title: Text("21 Dec 2022 at 13:00-15:50 Introduction to Natural Language Processing at room: IF-6T02"),
    subtitle: Text("21 Dec 2022 12:50"),
  );
}

Widget subject5() {
  return ListTile(
    leading: Icon(Icons.notifications_rounded),
    title: Text("21 Dec 2022 at 10:00-11:50 Moblie Application Development I at room: IF-4C01"),
    subtitle: Text("21 Dec 2022 09:46"),
  );
}

Widget subject6() {
  return ListTile(
    leading: Icon(Icons.notifications_rounded),
    title: Text("20 Dec 2022 at 15:00-16:50 Software Testing at room: IF-3C03"),
    subtitle: Text("20 Dec 2022 14:46"),
  );
}

Widget subject7() {
  return ListTile(
    leading: Icon(Icons.notifications_off),
    title: Text("20 Dec 2022 at 13:00-14:50 Object-Oriented Analysis and Design at room: IF-3C01"),
    subtitle: Text("20 Dec 2022 12:53"),
  );
}

Widget subject8() {
  return ListTile(
    leading: Icon(Icons.notifications_rounded),
    title: Text("20 Dec 2022 at 10:00-11:50 Multimedia Programming for Multiplatforms at room: IF-4C01"),
    subtitle: Text("20 Dec 2022 09:45"),
  );
}

Widget subject9() {
  return ListTile(
    leading: Icon(Icons.notifications_off),
    title: Text("19 Dec 2022 at 17:00-18:50 Web Programming at room: IF-3M210"),
    subtitle: Text("19 Dec 2022 16:46"),
  );
}

Widget subject10() {
  return ListTile(
    leading: Icon(Icons.notifications_rounded),
    title: Text("19 Dec 2022 at 13:00-14:50 Object-Oriented Analysis and Design at room: IF-3M210"),
    subtitle: Text("19 Dec 2022 12:48"),
  );
}

